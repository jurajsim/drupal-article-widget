'use strict';
import React from 'react';
import ReactDOM from 'react-dom';
import Article from './lib/component/article.jsx';
import axios from 'axios';

require('./scss/main.scss');

export default class Widget extends React.Component {

    constructor(props) {
        super(props);
        this.apiEndpoint = document.getElementById('widget').getAttribute('apiEndpoint')
        this.articleId = parseInt(document.getElementById('widget').getAttribute('articleId')),
            this.state = {}
    }

    componentWillUnmount() {
        this.serverRequest.abort();
    }

    componentDidMount() {
        var self = this;
        this.serverRequest = axios.get(this.apiEndpoint + this.articleId)
            .then(function (result) {
                self.setState({article: result.data});
            })
    }

    render() {
        if (this.state.article) {
            return (
                <Article article={this.state.article}></Article>
            );
        } else {
            return <div>Loading...</div>
        }
    }
}

ReactDOM.render(<Widget/>, document.getElementById('widget'));