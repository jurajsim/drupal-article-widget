'use strict';

export default class Language {
    static currentLangCode() {
        return location.pathname.indexOf('/en/') > -1 ? 'en' : 'cs';
    }
}