'use strict';
import React from 'react';

export default class LanguageSelector extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            value: this.props.value
        }
    }

    change = event => {
        this.state.value = event.target.value;
        this.props.onChange(event.target.value);
    };

    render() {
        return (
            <select className="lang-selector" value={this.state.value} onChange={this.change}>
                <option value="cs">Česky</option>
                <option value="en">English</option>
            </select>
        );
    }
}