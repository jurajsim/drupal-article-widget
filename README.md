# README #

Compatibility widget to integrate articles in drupal CMS

### How to use it ###

Add this code into any drupal page (type = PHP)
```html
<div id="widget" class="article-widget" articleId="__ARTICLE_ID__" apiEndpoint="__PUBLIC_API_ENDPOINT_FOR_ARTICLES__"/>
<script src="__WIDGET_JS_URL__"></script>
<script>
var link = document.createElement( "link" );
link.href = "__WIDGET_CSS_URL__"
link.type = "text/css";
link.rel = "stylesheet";
link.media = "screen,print";
document.getElementsByTagName( "head" )[0].appendChild( link );
</script>

```