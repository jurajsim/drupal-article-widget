var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var autoprefixer = require('autoprefixer');
var path = require('path');

var BUILD_DIR = path.resolve(__dirname, 'public');
var APP_DIR = path.resolve(__dirname, 'src/');
var SASS_DIR = path.resolve(__dirname, 'src/scss');

// var scssPlugin = new ExtractTextPlugin('public/css/styles.css', {
//     allChunks: true
// })

var getDEvTool = function() {
    if (process.env.NODE_ENV === 'production') {
        return 'cheap-module-source-map';
    } else {
        return 'eval';
    }
}

var config = {
    devtool: getDEvTool(),
    entry: APP_DIR + '/app.jsx',
    output: {
        path: BUILD_DIR,
        filename: 'js/bundle.js'
    },
    module: {
        loaders: [
            {
                test: /\.scss/i,
                loader: ExtractTextPlugin.extract('style-loader', 'css-loader!sass-loader'),
                include: SASS_DIR
            },
            {
                test: /\.js?/,
                include: APP_DIR,
                loader: 'babel-loader',
                query: {
                    presets: ["es2015", "react", "stage-0"],
                }
            }
        ],
    },
    postcss: [ autoprefixer({ browsers: ['last 2 versions'] }) ],
    plugins: [
        new ExtractTextPlugin('css/styles.css'),
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify('production')
            }
        })
    ],
};

module.exports = config;
